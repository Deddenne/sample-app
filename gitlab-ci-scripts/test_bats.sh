#!/usr/bin/env bats

@test "Check Home page" {
  run curl -sfq my-app:8000/
  [ "$status" -eq 0 ]
  [ "$(echo "$output" | fgrep -c Hello)" -gt 0 ]
}

@test "Check version URL" {
  run curl -sfq my-app:8000/version
  [ "$status" -eq 0 ]
}

@test "Check version URL Content" {
  run curl -sfq my-app:8000/version
  [ "$status" -eq 0 ]
  [ "$(echo "$output" | fgrep -c $CI_COMMIT_SHORT_SHA)" -gt 0 ]
}
